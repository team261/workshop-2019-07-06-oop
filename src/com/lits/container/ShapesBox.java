package com.lits.container;

import com.lits.interfaces.Shape;

import java.util.ArrayList;
import java.util.List;

public class ShapesBox {

    private List<Shape> shapes = new ArrayList<>();

    public void addShape(Shape shape) {
        System.out.println("Adding new shape [" + shape + "]");
        shapes.add(shape);
    }

    // returns sum square of all shapes in our shapebox object
    public int summarySquare() {

        int sum = 0;

        for (Shape shape : shapes) {
            sum += shape.square();
        }
        return sum;
    }

    // returns sum perimeter of all shapes in our shapebox object
    public int summaryPerimeter() {
        return shapes.stream().map(Shape::perimeter).reduce( (r,l) -> r + l).get();
    }
}
