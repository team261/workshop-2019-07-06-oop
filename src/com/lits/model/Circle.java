package com.lits.model;

import com.lits.interfaces.Shape;

public class Circle implements Shape {

    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public int square() {
        return (int) (Math.PI * radius * radius);
    }

    @Override
    public int perimeter() {
        return (int) (Math.PI * 2 * radius);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
