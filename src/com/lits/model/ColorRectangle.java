package com.lits.model;

public class ColorRectangle extends Rectangle {

    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // get RCG / HEX color
    public String getRGB () {

        String result = "Unknown";
        switch (color){

            case "RED":
                result = "rgb(12,0,0)";
                break;
            case "BLUE":
                result = "rgb(0,0, 10)";
                break;
            case "GREEN":
                result = "rgb(50,0,0)";
                break;
            case "BLACK":
                result = "rgb(0,0,0)";
                break;
        }
        return result;
    }

    @Override
    public String toString() {
        return "ColorRectangle{" +
                "color='" + color + '\'' +
                "} " + super.toString();
    }
}
