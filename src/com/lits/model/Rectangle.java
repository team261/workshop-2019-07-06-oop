package com.lits.model;

import com.lits.interfaces.Shape;

import java.time.LocalDateTime;
import java.util.Random;

// Rectangle implements all methods in Shape interface
// means all Shape can do - Rectangle also can do
public class Rectangle implements Shape {

    // private means accessible only in current class methods
    private int length;
    private int width;
    private LocalDateTime createdAt;


    // constructor with args
    public Rectangle(int length, int width) {

        this(); // call to first constructor that sets createAt field

        this.length = length; // set length
        this.width = width; // set width
    }

    // create constructor of Rectangle class
    public Rectangle () {

        // each time new Rectangle () created
        // createdAt value will be set to current time
        createdAt = LocalDateTime.now();
    }

    // uses for random gen lenth and width
    public Rectangle(boolean random) {

        this(); // call to first constructor

        Random r = new Random();
        width = r.nextInt(100); // return random value from 0 to 100
        length = 50 + r.nextInt(200); // return random value from 50 to 250
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    // check whether parrent class or interface has "int square()" method
    // new method that calculates square of Rectangle
    @Override
    public int square() {
        return length * width;
    }

    @Override
    // check whether parrent class or interface has "int perimeter()" method
    // new method that calculate perimeter of Rectangle
    public int perimeter() {
        return length + width + length + width;
    }


    @Override
    public String toString() {
        return "Rectangle{" +
                "length=" + length +
                ", width=" + width +
                ", createdAt=" + createdAt +
                '}';
    }
}
