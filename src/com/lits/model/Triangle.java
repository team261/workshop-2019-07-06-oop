package com.lits.model;

import com.lits.interfaces.Shape;

public class Triangle implements Shape {

    public static void print(Triangle triangle) {
        System.out.println("Call from staic context");
        System.out.println(triangle.toString());
    }


    private int sideA;
    private int sideB;
    private int sideC;

    public Triangle(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }
    @Override
    public int square() {
        int s = (sideA + sideB + sideC)/ 2;
        return (int) Math.sqrt( ( (s - sideA) * (s - sideB) * (s - sideC)) );
    }

    @Override
    public int perimeter() {
        return sideA + sideB + sideC;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "sideA=" + sideA +
                ", sideB=" + sideB +
                ", sideC=" + sideC +
                '}';
    }
}
