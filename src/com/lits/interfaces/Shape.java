package com.lits.interfaces;

public interface Shape {

    // return square of geometr shape
    int square();

    // return square of length of side line
    int perimeter();
}
