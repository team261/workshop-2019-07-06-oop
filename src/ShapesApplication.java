import com.lits.container.ShapesBox;
import com.lits.interfaces.Shape;
import com.lits.model.Circle;
import com.lits.model.Rectangle;
import com.lits.model.Triangle;

import java.util.Random;

public class ShapesApplication {

    public static void main(String[] args) {

        // shape is a reference to object of any class that implements Shape interface (can be Rectangle, Circle or Triangle etc...)
        Shape rect = new Rectangle(10, 20); // constructor with parameters
        // declaring as Shape but create as Triangle
        Shape triangle = new Triangle(3,4,5);
        // declaring as Shape but create as Triangle
        Shape circle = new Circle(7);



        ShapesBox shapesBox = new ShapesBox();

        // add to shape box by variable named 'rect' see line 12
        shapesBox.addShape(rect);
        // add to shape box by variable named 'triangle' see line 14
        shapesBox.addShape(triangle);
        // add to shape box by variable named 'circle' see line 16
        shapesBox.addShape(circle);

        // add to shape box without creating variable
        shapesBox.addShape(new Triangle(5,6,7));
        shapesBox.addShape(new Rectangle(6,7));
        shapesBox.addShape(new Circle(7));
        shapesBox.addShape(new Circle(6));

        System.out.println("Sum square: " + shapesBox.summarySquare());
        System.out.println("Sum perimeter: " + shapesBox.summaryPerimeter());



    }
}
